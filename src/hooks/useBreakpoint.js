import { useState, useEffect } from "react";

let breakpoints = {
    "tablet": 600,
    "desktop": 900,
    "lg-desktop": 1200
}

const useBreakpoint = () => {
    const [windowWidth, setWindowWidth] = useState(document.documentElement.clientWidth);

    useEffect(() => {
        window.addEventListener("resize", 
            () => setWindowWidth(document.documentElement.clientWidth)); 
    }, []);
     
    let device = "mobile"
    if (windowWidth >= breakpoints["tablet"] && windowWidth < breakpoints["desktop"]) {
        device = "tablet";
    } else if (windowWidth >= breakpoints["desktop"] && windowWidth < breakpoints["lg-desktop"]) {
        device = "desktop" ;
    } else if (windowWidth >= breakpoints["lg-desktop"]) {
        device = "lg-desktop";
    }

    return { device };
}

export default useBreakpoint;