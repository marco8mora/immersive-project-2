import { useState, useEffect } from "react";

const useFetch = (url) => {
    const [data, setData] = useState(null);

    useEffect(() => {
        const dataTimer = setTimeout( () => {
            let rawData = require('../assets/data/productsList.json');
            setData(rawData);
        }, 5000);

        return () => clearTimeout(dataTimer);
    }, []);
    
    return { data };
}

export default useFetch;