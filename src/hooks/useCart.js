import { useEffect, useState } from "react";
import { BehaviorSubject } from "rxjs";

let cart$ = null;

export const useCart = () => {
    const [ cart, setCart ] = useState();

    if (!cart$) { cart$ = new BehaviorSubject({}) }
    
    useEffect(() => {
        let sub = cart$.subscribe(list => {
            setCart(list);
        });

        return () => sub.unsubscribe();
    }, []);

    return { cart , cart$ };
};