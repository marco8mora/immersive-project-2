import PropTypes from 'prop-types';

const AlertContainer = props => {
    const block = "alert-container";
    const { type , children } = props;
    
    return (
        <div className={`${block}__root ${block}__root--${type}`}>
            {children}
        </div>
    )
}

AlertContainer.propTypes = {
    type: PropTypes.string.isRequired,
    children: PropTypes.any
};

export default AlertContainer;