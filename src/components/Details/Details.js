import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { useProductsList } from "../../contexts/ProductsListContext";
import { useCart } from "../../hooks/useCart";
import LargeImageCard from "../LargeImageCard/LargeImageCard";
import LoadingProductsAlert from "../LoadingProductsAlert/LoadingProductsAlert";
import QuantityInput from "../QuantityInput/QuantityInput";
import AlertContainer from "../AlertContainer/AlertContainer";

const Details = () => {
    const block = 'details';
    const { productId } = useParams();
    const productsList = useProductsList();
    let navigate = useNavigate();
    const [ product , setProduct ] = useState(null);
    const { cart , cart$ } = useCart();
    const [ quantity , setQuantity ] = useState(1);
    const [ displayAddedAlert , setDisplayAddedAlert ] = useState(false);

    useEffect(() => {
        if(productsList){
            const prod = productsList.find(p => p.productId === productId);
            if(!prod) {
                return navigate('/notfound');
            }

            setProduct(prod)
        }
    }, [productsList, productId, navigate])


    const changeQuantity = (productId,newValue) => {
        setQuantity(newValue);
    }

    const addToCart = () => {
        if(cart[productId]) {
            let previousAmount = cart[productId];
            cart$.next({...cart, [productId]: previousAmount + Number(quantity)})    
        } else {
            cart$.next({...cart, [productId]: Number(quantity)});
        }

        setDisplayAddedAlert(true);

        setTimeout( () => {
            setDisplayAddedAlert(false);
        }, 3000);
    }

    if (!product){
        return (
            <LoadingProductsAlert />
        );
    }
    
    return (
        <div className={`${block}__root`}>
            <div className={`${block}__back-btn-section`}>
                <Link className={`${block}__back-btn-section__link`} 
                to='/products'>
                    Go back
                </Link>
            </div>
            <div className={`${block}__card-container`}>
                <LargeImageCard product={product}/>
            </div>
            <div className={`${block}__description-container`}>
                <h1 className={`${block}__description-container__name`}>{product.name}</h1>
                <small className={`${block}__description-container__category`}>{product.category}</small>
                <p className={`${block}__description-container__summary`}>{product.summary}</p>
                <data className={`${block}__description-container__price`}>${product.price}<small> /unit</small></data>
                <QuantityInput defaultValue={1} onChangeHandler={changeQuantity}/>
                <button className={`${block}__add-cart-btn`} onClick={addToCart}>
                    Add to cart
                </button>
                { displayAddedAlert &&
                    <div className={`${block}__description-container__added-alert`}>
                        <AlertContainer type='success'>
                            <h2>Product added to cart</h2>
                        </AlertContainer>
                    </div>
                }
            </div>
        </div>
    );
}

export default Details;