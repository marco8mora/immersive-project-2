import PropTypes from 'prop-types';
import { useState } from "react";

const LargeImageCard = props => {
    const block = 'large-image-card';
    const { product } = props;
    const [ displayedImage , setDisplayedImage ] = useState('main');

    const changeDisplayedImage = (type) => {
        setDisplayedImage(type);
    };

    return (
        <div className={`${block}__root`}>
            <div className={`${block}__image-container`}>
                { displayedImage === 'main' &&
                    <img 
                        className={`${block}__image-container__img`} 
                        src={product.imageSrc} 
                        alt={product.imageAlt}/>
                }
                { displayedImage === 'secondary' &&
                    <img 
                    className={`${block}__image-container__img`} 
                    src={product.secondaryImageSrc} 
                    alt={product.imageAlt}/>
                }
            </div>
            <div className={`${block}__gallery-container`}>
                <button className={`${block}__image-button`} onClick={() => changeDisplayedImage('main')}>
                    <img 
                            className={`${block}__image-button__img`} 
                            src={product.imageSrc} 
                            alt={product.imageAlt}/>
                </button>
                {
                    product.secondaryImageSrc &&
                    <button className={`${block}__image-button`} onClick={() => changeDisplayedImage('secondary')}>
                        <img 
                        className={`${block}__image-button__img`} 
                        src={product.secondaryImageSrc} 
                        alt={product.imageAlt}/>
                    </button>
                }
            </div>
        </div>
    );
}

LargeImageCard.propTypes = {
    product: PropTypes.object.isRequired
};

export default LargeImageCard;