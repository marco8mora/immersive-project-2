import AlertContainer from "../AlertContainer/AlertContainer";
import Loader from "../Loader/Loader";

const LoadingProductsAlert = () => {
    const block = 'loading-products';

    return (
        <>
            <AlertContainer type='waiting'>
                <h1 className={`${block}__message`}>Loading products...</h1>
                <Loader />
            </AlertContainer>
        </>
    );
}

export default LoadingProductsAlert;