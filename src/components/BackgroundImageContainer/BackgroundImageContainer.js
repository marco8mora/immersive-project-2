const BackgroundImageContainer = props => {
    const block = 'bg-image-cont';
    const { children , imageSrc, minHeight } = props;
    
    return (
        <div className={`${block}__root`} style={{
            backgroundImage: `linear-gradient(rgb(0, 0, 0, 0.4), rgb(0, 0, 0, 0.4)), url(${imageSrc})`,
            minHeight: minHeight
        }}>
            {children}
        </div>
    );

}

export default BackgroundImageContainer;