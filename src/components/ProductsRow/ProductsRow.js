import PropTypes from 'prop-types';
import ProductCard from "../ProductCard/ProductCard";

const ProductsRow = props => {
    const block = 'products-row';
    const { rowTitle , productsList } = props;

    let productCards = productsList.map(product => 
        <ProductCard key={product.productId}
        productId={product.productId} 
        name={product.name} 
        category={product.category}
        imageSrc={product.imageSrc}
        imageAlt={product.imageAlt}
        summary={product.summary}
        price={product.price}/>    
    )
    
    return(
        <div className={`${block}__root`}>
            <h2 className={`${block}__title`}>{rowTitle}</h2>
            <div className={`${block}__row`}>
                {productCards}
            </div>
        </div>
    );
}

ProductsRow.propTypes = {
    rowTitle: PropTypes.string.isRequired,
    productsList: PropTypes.array.isRequired
};

export default ProductsRow;