import PropTypes from 'prop-types';
import { ReactComponent as Plus } from "../../assets/svg/plus.svg";
import { ReactComponent as Minus } from "../../assets/svg/minus.svg";

const QuantityInput = props => {
    const block = 'quantity-input';
    const { id, defaultValue, onChangeHandler } =  props;

    const addInputValue = () => {
        const inputId = '#quantityInput' + id;
        const input = document.querySelector(inputId);
        
        Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, 'value')
        .set.call(input, Number(input.value) + 1);

        input.dispatchEvent(new Event('change', { bubbles: true }));
    }

    const substractInputValue = () => {
        const inputId = '#quantityInput' + id;
        const input = document.querySelector(inputId);
        
        Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, 'value')
        .set.call(input, Number(input.value) - 1);

        input.dispatchEvent(new Event('change', { bubbles: true }));
    }

    return (
        <div className={`${block}__root`}>
            <label className={`${block}__label`} htmlFor="quantityInput">Quantity:</label>
            <button className={`${block}__button`} onClick={substractInputValue} aria-label='Substract one to quantity'>
                <Minus />
            </button>
            <input 
            id={'quantityInput' + id}
            className={`${block}__input`} 
            type='number'
            defaultValue={defaultValue}
            pattern='[0-9]*'
            min={1}
            aria-label='Product quantity'
            onChange={(event) => {
                if(Number(event.target.value) <= 0){
                    event.target.value = 1
                }
                onChangeHandler(id,event.target.value)
                }}/>
            <button className={`${block}__button`} onClick={addInputValue} aria-label='Substract one to quantity'>
                <Plus />
            </button>
        </div>
    );
};

QuantityInput.propTypes = {
    id: PropTypes.string.isRequired,
    defaultValue: PropTypes.number,
    onChangeHandler: PropTypes.func.isRequired
};

export default QuantityInput;