import BackgroundImageContainer from "../BackgroundImageContainer/BackgroundImageContainer";
import { ReactComponent as Logo } from "../../assets/svg/logo.svg";

const Hero = () => {
    const block = 'hero';
    
    return (
        <div className={`${block}__root`}>
            <BackgroundImageContainer
                imageSrc='https://cdn.shopify.com/s/files/1/0433/7047/9773/files/portada_1_copy_1944x.jpg?v=1631206369'
                minHeight='100vh'
            >
                <div className={`${block}__logo`}>
                    <Logo />
                </div>
                <h1 className={`${block}__main-title`}>Royal Accessories</h1>
            </BackgroundImageContainer>
            <div className={`${block}__sub-section`}>
                <h2 className={`${block}__sub-section__title`}>Royalty Quality</h2>
                <p className={`${block}__sub-section__description`}>We offer all kinds of high quality accessories: <strong>rings, earrings, necklaces, bracelets, watches, and much more!</strong>. Designs for every taste and preference to complete your perfect look.</p>
            </div>
            <BackgroundImageContainer
                imageSrc='https://cdn.shopify.com/s/files/1/0433/7047/9773/files/portada_3_1080x.jpg?v=1631206668'
                minHeight='300px'
            >
                <h3 className={`${block}__quote`}>
                    <q>The treasure every king and queen needs</q>
                </h3>
            </BackgroundImageContainer>
            <div className={`${block}__sub-section`}>
                <h2 className={`${block}__sub-section__title`}>Stores</h2>
                <ul className={`${block}__sub-section__list`}>
                    <li className={`${block}__sub-section__list-item`}>
                        Los Angeles, California
                    </li>
                    <li className={`${block}__sub-section__list-item`}>
                        New York City, New York
                    </li>
                    <li className={`${block}__sub-section__list-item`}>
                        Austin, Texas
                    </li>
                    <li className={`${block}__sub-section__list-item`}>
                        Chicago, Illinois
                    </li>
                    <li className={`${block}__sub-section__list-item`}>
                        Seattle, Washington
                    </li>
                </ul>
            </div>
            <BackgroundImageContainer
                imageSrc='https://cdn.shopify.com/s/files/1/0433/7047/9773/files/portada_4_1080x.jpg?v=1631207147'
                minHeight='300px'
            >
                <h3 className={`${block}__quote`}>
                    <q>Best gear and style on the market</q>
                </h3>
            </BackgroundImageContainer>
        </div>
    );
}

export default Hero;