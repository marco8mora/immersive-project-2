import { useState } from "react";
import { NavLink } from "react-router-dom";

const HamButton = props => {
    const block = 'ham-btn'
    const [ isOpen , setIsOpen ] = useState(false);
    const { itemsInCart } = props;

    const toggleMenu = () => {
        setIsOpen(status => !status);
    };

    return (
        <div className={`${block}__root`}>
            <button className={`${block}__button`} onClick={toggleMenu}>
                <svg viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g id="hamburger">
                        <g id="hamLines" className={!isOpen ? "ham-btn--visible" : "ham-btn--not-visible"}>
                            <rect id="hamLinesRect1" x="19" y="19" width="62" height="7" rx="3.5" fill="white"/>
                            <rect id="hamLinesRect2" x="19" y="73" width="62" height="7" rx="3.5" fill="white"/>
                            <rect id="hamLinesRect3" x="4" y="46" width="93" height="7" rx="3.5" fill="white"/>
                        </g>
                        <g id="hamX" className={isOpen ? "ham-btn--visible" : "ham-btn--not-visible"}>
                            <rect id="hamXRect1" x="10.6129" y="4.66315" width="120.307" height="7" rx="3.5" transform="rotate(45 10.6129 4.66315)" fill="white"/>
                            <rect id="hamXRect2" x="95.6828" y="9.61288" width="120.307" height="7" rx="3.5" transform="rotate(135 95.6828 9.61288)" fill="white"/>
                        </g>
                    </g>
                </svg>
            </button>
            { isOpen && 
                <div className={`${block}__menu`}>
                    <ul className={`${block}__list`}>
                        <li className={`${block}__list-item`}>
                            <NavLink 
                            className={({ isActive }) => isActive ? `${block}__link ${block}__link--active` : `${block}__link`}
                            to='/' onClick={toggleMenu}>
                                HOME
                            </NavLink>
                        </li>
                        <li className={`${block}__list-item`}>
                            <NavLink 
                            className={({ isActive }) => isActive ? `${block}__link ${block}__link--active` : `${block}__link`}
                            to='/products' onClick={toggleMenu}>
                                PRODUCTS
                            </NavLink>
                        </li>
                        <li className={`${block}__list-item`}>
                            <NavLink 
                            className={({ isActive }) => isActive ? `${block}__link ${block}__link--active` : `${block}__link`}
                            to='/cart' onClick={toggleMenu}>
                                CART
                                { itemsInCart !== 0 &&
                                    <span>: {itemsInCart} items</span>
                                }
                            </NavLink>
                        </li>
                    </ul>
                </div>
            }
        </div>
    );
}

export default HamButton;