import { Outlet } from 'react-router-dom';
import Navbar from '../Navbar/Navbar';
import { ProductsListProvider } from '../../contexts/ProductsListContext';

function App() {
  return (
    <>
        <header>
          <Navbar />
        </header>
        <main>
          <ProductsListProvider>
            <Outlet/>
          </ProductsListProvider>
        </main> 
    </>
  );
}

export default App;
