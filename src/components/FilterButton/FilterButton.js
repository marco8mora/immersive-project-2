import PropTypes from 'prop-types';

const FilterButton = props => {
    const block = 'filter-button';
    const { category , onClickHandler , active } = props

    return (
        <div className={`${block}__root`}>
            <button className={`${block}__button ${active ? 'filter-button__button--active' : ''}`} onClick={() => onClickHandler(category)}>
                {category}
            </button>
        </div>
    ); 
}

FilterButton.propTypes = {
    category: PropTypes.string.isRequired,
    onClickHandler: PropTypes.func.isRequired,
    active: PropTypes.bool
};

export default FilterButton;