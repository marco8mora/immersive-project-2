import PropTypes from 'prop-types';
import { ReactComponent as XSymbol } from "../../assets/svg/x-symbol.svg";

const DeleteButton = props => {
    const block = 'delete-button';
    const { productId, name, onClickHandler } = props;

    return (
        <div className={`${block}__root`}>
            <button className={`${block}__button`} 
            onClick={() => onClickHandler(productId)}
            aria-label={`Delete product ${name} from cart`}>
                <XSymbol />
            </button>
        </div>
    );
};

DeleteButton.propTypes = {
    productId: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    onClickHandler: PropTypes.func.isRequired
};

export default DeleteButton;