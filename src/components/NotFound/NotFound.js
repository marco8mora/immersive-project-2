const NotFound = () => {
    const block = 'not-found';

    return (
        <>
            <h1 className={`${block}__main-title`}>
                Page not found
            </h1>
        </>
    );
};

export default NotFound;