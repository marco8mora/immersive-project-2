import { Link } from "react-router-dom";
import { useCart } from "../../hooks/useCart";
import { useProductsList } from "../../contexts/ProductsListContext";
import LoadingProductsAlert from "../LoadingProductsAlert/LoadingProductsAlert";
import AlertContainer from "../AlertContainer/AlertContainer";
import QuantityInput from "../QuantityInput/QuantityInput";
import DeleteButton from "../DeleteButton/DeleteButton";

const Cart = () => {
    const block = 'cart';
    const { cart , cart$ } = useCart();
    const productsList = useProductsList();
    
    const calculateSubtotal = () => {
        let totalSum = 0;
        Object.entries(cart).forEach(productInCart => {
            let unitPrice = Number(productsList.find(p => p.productId === productInCart[0]).price) 
            totalSum += unitPrice * productInCart[1];
        });

        return totalSum;
    }
    
    const changeQuantity = (productId, newValue) => {
        cart$.next({...cart , [productId]: Number(newValue)});
    };

    const deleteProduct = productId => {
        let newCart = {};
        Object.entries(cart).forEach(product => {
            if(product[0] !== productId){
                newCart[product[0]] = Number(product[1]);
            }
        })
        cart$.next(newCart);
    }
    
    const getProduct = productId => {
        return productsList.find(p => p.productId === productId);
    }
    
    if (!productsList || !cart){
        return (
            <LoadingProductsAlert />
        );
    }

    if (Object.entries(cart).length === 0){
        return (
            <>
                <h1 className={`${block}__main-title`}>My Cart</h1>
                <AlertContainer type='error'>
                    <h2 style={{width: '100%', textAlign: 'center'}}>No products currently in cart</h2>
                </AlertContainer>
            </>
        );
    }


    const productsInTable = Object.entries(cart).map(productInCart => 
        <tr className={`${block}__table-body__product-row`} key={productInCart[0]}>
            <td className={`${block}__product-cell`}>    
                <DeleteButton productId={productInCart[0]} name={getProduct(productInCart[0]).name} onClickHandler={deleteProduct}/>
                <img 
                    className={`${block}__product-image`}
                    src={getProduct(productInCart[0]).imageSrc} 
                    alt={getProduct(productInCart[0]).imageAlt}/>
                <Link 
                className={`${block}__product-link`}
                to={`/details/${productInCart[0]}`}>
                    {getProduct(productInCart[0]).name}
                </Link>
                <div className={`${block}__product-quantity`}>
                    <QuantityInput defaultValue={productInCart[1]} id={productInCart[0]} onChangeHandler={changeQuantity}/>
                </div>
            </td>
            <td>
                ${getProduct(productInCart[0]).price * productInCart[1]}
            </td>
        </tr>
    );
    
    return (
        <div className={`${block}__root`}>
            <h1 className={`${block}__main-title`}>My Cart</h1>
            <table className={`${block}__table`}>
                <thead className={`${block}__table-header`}>
                    <tr>
                        <th>Product</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody className={`${block}__table-body`}>
                    {productsInTable}
                    <tr className={`${block}__table-body__subtotal-row`}>
                        <td>
                            Subtotal 
                        </td>
                        <td>
                            ${calculateSubtotal()}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div className={`${block}__btn-container`}>
                <Link className={`${block}__btn-container__checkout-btn`} 
                    to='/checkout'>
                        Continue to checkout
                </Link>
            </div>
        </div>
    );
};

export default Cart;