import { useProductsList } from "../../contexts/ProductsListContext";
import { useEffect, useState } from "react";
import Hero from "../Hero/Hero";
import ProductsRow from "../ProductsRow/ProductsRow";
import LoadingProductsAlert from "../LoadingProductsAlert/LoadingProductsAlert";
import AlertContainer from "../AlertContainer/AlertContainer";

const Home = () => {
    const block = 'home';
    const productsList = useProductsList();
    const [ displayThanks, setDisplayThanks ] = useState(false);

    useEffect(() => {
        if(localStorage.getItem('purchase-confirmed')) {
            setDisplayThanks(true);
            localStorage.removeItem('purchase-confirmed');

            setTimeout( () => {
                setDisplayThanks(false);
            }, 3000);
        }
    } , [displayThanks])
    
    return (
        <>  { displayThanks &&
                <div className={`${block}__thank-alert`}>
                    <AlertContainer type='success'>
                        <h2>Purchase confirmed, thank you for choosing our products!</h2>
                    </AlertContainer>
                </div>
            }
            <Hero />
            {
                (!productsList && 
                    <LoadingProductsAlert />
                ) 
                || <ProductsRow 
                    rowTitle='Featured products'  
                    productsList={productsList.filter(product => product.featured )}/>
            }
        </>
    );
};

export default Home;