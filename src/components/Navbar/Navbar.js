import { Link, NavLink } from "react-router-dom";
import useBreakpoint from "../../hooks/useBreakpoint";
import HamButton from "../HamButton/HamButton";
import { ReactComponent as Logo } from "../../assets/svg/logo.svg";
import { ReactComponent as CartIcon } from "../../assets/svg/shopping-cart.svg";
import { useCart } from "../../hooks/useCart";
import { useEffect, useState } from "react";

const Navbar = () => {
    const block = 'navbar';
    const { device } = useBreakpoint();
    const { cart } = useCart();
    const [ itemsInCart , setItemsInCart ] = useState(null);

    useEffect(() => {
        if (cart) {
            let totalItems = 0;
            Object.values(cart).forEach(quantity => {
                totalItems += Number(quantity);
            })
            setItemsInCart(totalItems);
        } else {
            setItemsInCart(0);
        }
    }, [cart]);

    return (
        <nav className={`${block}__root`}>
            <div className={`${block}__logo-section`}>
                <Link className={`${block}__logo-section__link`} to='/' aria-label="Go to home page">
                    <Logo />
                </Link>
            </div>
            {   device === 'mobile' && <>
                    <div className={`${block}__ham-btn-section`}>
                        <HamButton itemsInCart={itemsInCart}/>
                    </div>
                </>
            }
            {   device !== 'mobile' && <>
                    <div className={`${block}__links-section`}>
                        <ul className={`${block}__links-section__list`}>
                            <li className={`${block}__links-section__list-item`}>
                                <NavLink 
                                className={({ isActive }) => isActive ? `${block}__links-section__link ${block}__links-section__link--active` : `${block}__links-section__link`}
                                to='/'>
                                    HOME
                                </NavLink>
                            </li>
                            <li className={`${block}__links-section__list-item`}>
                                <NavLink 
                                className={({ isActive }) => isActive ? `${block}__links-section__link ${block}__links-section__link--active` : `${block}__links-section__link`}
                                to='/products'>
                                    PRODUCTS
                                </NavLink>
                            </li>    
                        </ul>
                    </div>
                    <div className={`${block}__cart-section`}>
                        <Link className={`${block}__cart-section__link`} to='/cart' aria-label='Go to cart page'>
                            <CartIcon />
                            { itemsInCart !== 0 &&
                                <span className={`${block}__cart-section__amount`}>{itemsInCart}</span>
                            }
                        </Link>
                    </div>
                </>
            }
        </nav>
    );
}

export default Navbar;