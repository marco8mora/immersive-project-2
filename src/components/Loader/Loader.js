const Loader = () => {
    const block = 'loader'

    return (
        <div className={`${block}__root`}>
            <span className={`${block}__span`}></span>
        </div>
    );
}

export default Loader;