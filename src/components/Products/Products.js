import { useState } from "react";
import { useProductsList } from "../../contexts/ProductsListContext";
import ProductsRow from "../ProductsRow/ProductsRow";
import AlertContainer from "../AlertContainer/AlertContainer";
import LoadingProductsAlert from "../LoadingProductsAlert/LoadingProductsAlert";
import FilterButton from "../FilterButton/FilterButton";

const Products = () => {
    const block = 'products';
    const productsList = useProductsList();
    const [ filters , setFilters ] = useState(
        {
            'Rings': true,
            'Pendants': true,
            'Bracelets': true,
            'Necklaces': true
        }
    );
    const [ allFiltersDisabled , setAllFiltersDisabled ] = useState(false);

    const toggleCategory = cat => {
        let newState = {...filters };
        newState[cat] = !newState[cat];

        setFilters(newState);

        if(Object.values(newState).find(element => element)) {
            setAllFiltersDisabled(false);
        } else {
            setAllFiltersDisabled(true);
        }
    } 

    const renderCategory = (cat) => {
        return (
            filters[cat] &&
            <ProductsRow 
                rowTitle={cat}  
                productsList={productsList.filter(product => product.category === cat )}/>
        );
    }

    return (
        <>
            <h1 className={`${block}__main-title`}>Products</h1>
            <div className={`${block}__filter-list`}>
                <h2 className={`${block}__filter-list__title`}>Filter products:</h2>
                <FilterButton category={'Rings'} onClickHandler={toggleCategory} active={filters['Rings']}/>
                <FilterButton category={'Pendants'} onClickHandler={toggleCategory} active={filters['Pendants']}/>
                <FilterButton category={'Bracelets'} onClickHandler={toggleCategory} active={filters['Bracelets']}/>
                <FilterButton category={'Necklaces'} onClickHandler={toggleCategory} active={filters['Necklaces']}/>
            </div>
            {
                (!productsList && 
                    <LoadingProductsAlert />
                ) 
                || 
                (!allFiltersDisabled &&
                    <>
                        {renderCategory('Rings')}
                        {renderCategory('Pendants')}
                        {renderCategory('Bracelets')}
                        {renderCategory('Necklaces')}
                    </> 
                )
                ||
                <>
                    <AlertContainer type='error'>
                        <h2 style={{width: '100%', textAlign: 'center'}}>All filters disabled, enable a filter to see products of that category</h2>
                    </AlertContainer>
                </>
            }
        </>
            
    );
}

export default Products;