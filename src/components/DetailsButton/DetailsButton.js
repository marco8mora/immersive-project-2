import { Link } from "react-router-dom";

const DetailsButton = props => {
    const block = 'details-btn';
    const { productId , name } = props;

    return (
        <div className={`${block}__root`}>
            <Link className={`${block}__link`} 
            to={`/details/${productId}`}
            aria-label={`Go to ${name} details page`}>
                DETAILS
            </Link>
        </div>
    );
}

export default DetailsButton;