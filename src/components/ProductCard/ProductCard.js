import DetailsButton from "../DetailsButton/DetailsButton";

const ProductCard = props => {
    const block = 'product-card';
    const { productId, name, category , imageSrc , imageAlt,
        summary , price } = props;

    return (
        <div className={`${block}__root`}>
            <h3 className={`${block}__name`}>{name}</h3>
            <small className={`${block}__category`}>{category}</small>
            <img className={`${block}__image`} src={imageSrc} alt={imageAlt}/>
            <p className={`${block}__summary`}>{summary}</p>
            <data className={`${block}__price`}>{`$${price}`}</data>
            <DetailsButton productId={productId} name={name}/>
        </div>
    );
}

export default ProductCard;