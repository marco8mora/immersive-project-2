import { useNavigate } from "react-router-dom";
import { useCart } from "../../hooks/useCart";
import { useProductsList } from "../../contexts/ProductsListContext";
import LoadingProductsAlert from "../LoadingProductsAlert/LoadingProductsAlert";

const Checkout = () => {
    const block = 'checkout';
    let navigate = useNavigate();
    const { cart , cart$ } = useCart();
    const productsList = useProductsList();

    const confirmPurchase = () => {
        cart$.next({});
        localStorage.setItem('purchase-confirmed', '1');
        return navigate('/');
    };

    const calculateSubtotal = () => {
        let totalSum = 0;
        Object.entries(cart).forEach(productInCart => {
            let unitPrice = Number(productsList.find(p => p.productId === productInCart[0]).price) 
            totalSum += unitPrice * productInCart[1];
        });

        return totalSum;
    }

    const getProduct = productId => {
        return productsList.find(p => p.productId === productId);
    }

    if (!productsList || !cart){
        return (
            <LoadingProductsAlert />
        );
    }

    if (Object.entries(cart).length === 0){
        return navigate('/cart');
    }

    const productsInTable = Object.entries(cart).map(productInCart => 
        <tr className={`${block}__table-body__product-row`} key={productInCart[0]}>
            <td className={`${block}__product-cell`}>    
                {getProduct(productInCart[0]).name} x {productInCart[1]}
            </td>
            <td>
                ${getProduct(productInCart[0]).price * productInCart[1]}
            </td>
        </tr>
    );
    
    return (
        <div className={`${block}__root`}>
            <h1 className={`${block}__main-title`}>Checkout</h1>
            <table className={`${block}__table`}>
                <thead className={`${block}__table-header`}>
                    <tr>
                        <th>Product</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody className={`${block}__table-body`}>
                    {productsInTable}
                    <tr className={`${block}__table-body__subtotal-row`}>
                        <td>
                            Subtotal 
                        </td>
                        <td>
                            ${calculateSubtotal()}
                        </td>
                    </tr>
                    <tr className={`${block}__table-body__tax-row`}>
                        <td>
                            + IVA (13%)
                        </td>
                        <td>
                            ${Math.round(calculateSubtotal() * 0.13).toFixed(2)}
                        </td>
                    </tr>
                    <tr className={`${block}__table-body__total-row`}>
                        <td>
                            Total
                        </td>
                        <td>
                            ${calculateSubtotal() + Number(Math.round(calculateSubtotal() * 0.13).toFixed(2))}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div className={`${block}__btn-container`}>
                <button 
                className={`${block}__btn-container__confirm-btn`} 
                onClick={confirmPurchase}>Confirm purchase</button>
            </div>
        </div>
    );
}

export default Checkout;