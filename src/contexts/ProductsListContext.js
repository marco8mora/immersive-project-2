import React, { useState, useEffect, useContext } from 'react'
import useFetch from '../hooks/useFetch'

const ProductsListContext = React.createContext();

export function ProductsListProvider({ children }) {
    const { data } = useFetch("api.com/products");
    const [ productsList, setProductsList ] = useState(null);

    useEffect(() => {
        setProductsList(data);
    }, [data]);

    return (
        <ProductsListContext.Provider value={productsList}>
            {children}
        </ProductsListContext.Provider>
    );
};

export function useProductsList() {
    return useContext(ProductsListContext);
};